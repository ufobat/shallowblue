package de.senfdax.ufobat.shallowblue.piece

import de.senfdax.ufobat.shallowblue.Piece
import de.senfdax.ufobat.shallowblue.Position
import de.senfdax.ufobat.shallowblue.Move
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class QueenTest {

    @Test
    fun queenCornerMoves() {
        val queen = Queen(Position.h8, Piece.WHITE)
        val moves = queen.pseudoLegalMoves()

        val expectation = listOf(
            Position.h1, Position.h2, Position.h3, Position.h4, Position.h5, Position.h6, Position.h7,
            Position.a8, Position.b8, Position.c8, Position.d8, Position.e8, Position.f8, Position.g8,
            Position.a1, Position.b2, Position.c3, Position.d4, Position.e5, Position.f6, Position.g7,
        )
            .map { Move(Position.h8, it) }
            .toSet()

        // assertEquals(expectation.count(), moves.count())
        assertEquals(expectation, moves.toSet() )
    }

    @Test
    fun queenCornerBlockedMoves() {
        val queen = Queen(Position.h8, Piece.WHITE)
        val whitePawn1 = Pawn(Position.e8, Piece.WHITE)
        val whitePawn2 = Pawn(Position.h7, Piece.WHITE)
        val blackPawn = Pawn(Position.e5, Piece.BLACK)
        val white = mapOf(queen.position to queen, whitePawn1.position to whitePawn1, whitePawn2.position to whitePawn2)
        val black = mapOf(blackPawn.position to blackPawn)
        val moves = queen.pseudoLegalMoves(white, black)

        val expectation = listOf(
            Position.g8, Position.g7, Position.f8, Position.f6, Position.e5
        ).map { Move(Position.h8, it) }.toSet()

        assertEquals(expectation, moves.toSet())
    }

}
