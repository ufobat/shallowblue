package de.senfdax.ufobat.shallowblue.piece

import de.senfdax.ufobat.shallowblue.Piece
import de.senfdax.ufobat.shallowblue.Position
import de.senfdax.ufobat.shallowblue.Move
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class RookTest {

    @Test
    fun rookCornerMoves() {
        val rook = Rook(Position.a8, Piece.WHITE)
        val moves = rook.pseudoLegalMoves()

        val expectation = listOf(
            Position.a1, Position.a2, Position.a3, Position.a4, Position.a5, Position.a6, Position.a7,
            Position.b8, Position.c8, Position.d8, Position.e8, Position.f8, Position.g8, Position.h8,
        ).map { Move(Position.a8, it) }.toSet()

        assertEquals(expectation, moves.toSet())
    }

    @Test
    fun rookMiddleMoves() {
        val rook = Rook(Position.b3, Piece.WHITE)
        val moves = rook.pseudoLegalMoves()

        val expectation = listOf(
            Position.b1, Position.b2, Position.b4, Position.b5, Position.b6, Position.b7, Position.b8,
            Position.a3, Position.c3, Position.d3, Position.e3, Position.f3, Position.g3, Position.h3
        ).map { Move(Position.b3, it) }.toSet()

        assertEquals(expectation, moves.toSet())
    }

    @Test
    fun moveCapturesKing() {
        val white = listOf(King(Position.a3, Piece.WHITE), Pawn(Position.b4, Piece.WHITE))
        val black = listOf(Rook(Position.c3, Piece.BLACK))

        val result = black[0].pseudoLegalMoves(
            black.associateBy { it.position },
            white.associateBy { it.position },
        )

        val expectation = listOf(
            Position.c2, Position.c1, Position.c4, Position.c5, Position.c6, Position.c7, Position.c8,
            Position.b3, Position.a3, Position.d3, Position.e3, Position.f3, Position.g3, Position.h3
        ).map { Move(Position.c3, it) }.toSet()
        assertEquals(expectation, result.toSet())
    }
}
