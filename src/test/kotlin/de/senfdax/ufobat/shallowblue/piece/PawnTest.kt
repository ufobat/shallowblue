package de.senfdax.ufobat.shallowblue.piece

import de.senfdax.ufobat.shallowblue.Move
import de.senfdax.ufobat.shallowblue.Piece.Companion.BLACK
import de.senfdax.ufobat.shallowblue.Piece.Companion.WHITE
import de.senfdax.ufobat.shallowblue.Position
import de.senfdax.ufobat.shallowblue.Position.*
import de.senfdax.ufobat.shallowblue.Promotion
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

class PawnTest {

    @Test
    fun randomWhitePawnMoves() {
        val pawn = Pawn(e4, WHITE)
        val pseudoLegalMoves = pawn.pseudoLegalMoves()
        pseudoLegalMoves.forEach {
            assertTrue(it is Move)
        }
        assertEquals(
            listOf(e5),
            pseudoLegalMoves.map { it.dest }.sorted()
        )
    }

    @Test
    fun randomBlackPawnMoves() {
        val pawn = Pawn(e4, BLACK)
        val pseudoLegalMoves = pawn.pseudoLegalMoves()
        assertEquals(1, pseudoLegalMoves.count())
        assertEquals(
            listOf(e3),
            pseudoLegalMoves.map { it.dest }
        )
    }

    @Test
    fun randomBlackPawnMovesWithOpponent() {
        val pawn = Pawn(e4, BLACK)
        val pseudoLegalMoves = pawn.pseudoLegalMoves(
            mapOf(),
            mapOf(d3 to Pawn(d3, WHITE)),
        )
        assertEquals(
            listOf(e3, d3).sorted(),
            pseudoLegalMoves.map { it.dest }.sorted()
        )
    }

    @Test
    fun blockedPawn() {
        val pawn = Pawn(e4, BLACK)
        val pseudoLegalMoves = pawn.pseudoLegalMoves(
            mapOf(),
            mapOf(e3 to Pawn(e3, WHITE)),
        )
        assertEquals(
            listOf<Position>(),
            pseudoLegalMoves.map { it.dest }
        )
    }

    @Test
    fun blockedPawnByOpponentAtStartingPos() {
        val pawn = Pawn(e7, BLACK)
        val pseudoLegalMoves = pawn.pseudoLegalMoves(
            mapOf(),
            mapOf(e6 to Pawn(e6, WHITE)),
        )
        assertEquals(
            listOf<Position>(),
            pseudoLegalMoves.map { it.dest }
        )
    }

    @Test
    fun blockedPawnBySameTeamAtStartingPos() {
        val pawn = Pawn(e7, BLACK)
        val pseudoLegalMoves = pawn.pseudoLegalMoves(
            mapOf(e6 to Pawn(e6, BLACK)),
            mapOf()
        )
        assertEquals(
            listOf<Position>(),
            pseudoLegalMoves.map { it.dest }
        )
    }

    @Test
    fun startingWhitePawnMoves() {
        val pawn = Pawn(e2, WHITE)
        val pseudoLegalMoves = pawn.pseudoLegalMoves()
        assertEquals(
            listOf(e3, e4).sorted(),
            pseudoLegalMoves.map { it.dest }.sorted()
        )
    }

    @Test
    fun startingBlackPawnMoves() {
        val pawn = Pawn(e7, BLACK)
        val pseudoLegalMoves = pawn.pseudoLegalMoves()
        assertEquals(
            listOf(e6, e5).sorted(),
            pseudoLegalMoves.map { it.dest }.sorted()
        )
    }

    @Test
    fun promotingWhitePawnAtTheSide() {
        val pawn = Pawn(h7, WHITE)
        val pseudoLegalMoves = pawn.pseudoLegalMoves()
        assertEquals(
            setOf(
                Move(h7, h8, Promotion.Queen),
                Move(h7, h8, Promotion.Rook),
                Move(h7, h8, Promotion.Bishop),
                Move(h7, h8, Promotion.Knight),
            ),
            pseudoLegalMoves.toSet()
        )
    }

    @Test
    fun promotingBlackPawnAtTheSide() {
        val pawn = Pawn(h2, BLACK)
        val pseudoLegalMoves = pawn.pseudoLegalMoves()
        assertEquals(
            setOf(
                Move(h2, h1, Promotion.Queen),
                Move(h2, h1, Promotion.Rook),
                Move(h2, h1, Promotion.Bishop),
                Move(h2, h1, Promotion.Knight),
            ),
            pseudoLegalMoves.toSet()
        )

    }

    @Test
    fun promotingWhitePawnWithTakingOpponent() {
        val pawn = Pawn(g7, WHITE)
        val me = mapOf(pawn.position to pawn)
        val opponents = mapOf(h8 to Rook(h8, BLACK))
        val pseudoLegalMoves = pawn.pseudoLegalMoves(me, opponents)
        assertEquals(
            setOf(
                Move(g7, h8, Promotion.Queen),
                Move(g7, h8, Promotion.Rook),
                Move(g7, h8, Promotion.Bishop),
                Move(g7, h8, Promotion.Knight),
                Move(g7, g8, Promotion.Queen),
                Move(g7, g8, Promotion.Rook),
                Move(g7, g8, Promotion.Bishop),
                Move(g7, g8, Promotion.Knight),
            ),
            pseudoLegalMoves.toSet()
        )
    }


    @Test
    fun promotingWhitePawnWithOpponent() {
        val pawn = Pawn(d7, WHITE)
        val rooks = mapOf(e8 to Rook(e8, BLACK), c8 to Rook(c8, BLACK))
        val pseudoLegalMoves = pawn.pseudoLegalMoves(mapOf(pawn.position to pawn), rooks )
        assertEquals(
            setOf(
                Move(d7, d8, Promotion.Queen),
                Move(d7, d8, Promotion.Rook),
                Move(d7, d8, Promotion.Bishop),
                Move(d7, d8, Promotion.Knight),
                Move(d7, c8, Promotion.Queen),
                Move(d7, c8, Promotion.Rook),
                Move(d7, c8, Promotion.Bishop),
                Move(d7, c8, Promotion.Knight),
                Move(d7, e8, Promotion.Queen),
                Move(d7, e8, Promotion.Rook),
                Move(d7, e8, Promotion.Bishop),
                Move(d7, e8, Promotion.Knight),
            ),
            pseudoLegalMoves.toSet()
        )
    }

    @Test
    fun enPassantAttack() {
        val whitePawn = Pawn(f5, WHITE)
        val blackPawn = Pawn(g5, BLACK)
        val black = listOf(blackPawn).associateBy { it.position }

        val moves = whitePawn.pseudoLegalMoves(mapOf(), black)
        val expectation = listOf(Move(f5, f6), Move(f5, g6)).toSet()
        assertEquals(expectation, moves.toSet())
    }
}
