package de.senfdax.ufobat.shallowblue

import de.senfdax.ufobat.shallowblue.piece.*
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

class FENParserTest {

    @Test
    fun startingPosition() {
        val fen = """rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1"""
        val board = FENParser.parse(fen)

        assertNotNull(board)
        assertEquals(Piece.WHITE, board.whoIsNext)
        assertEquals(16, board.whitePieces.count())
        assertEquals(16, board.blackPieces.count())

        // white pawns
        assertEquals(Pawn(Position.a2, Piece.WHITE), board.whitePieces[Position.a2])
        assertEquals(Pawn(Position.b2, Piece.WHITE), board.whitePieces[Position.b2])
        assertEquals(Pawn(Position.c2, Piece.WHITE), board.whitePieces[Position.c2])
        assertEquals(Pawn(Position.d2, Piece.WHITE), board.whitePieces[Position.d2])
        assertEquals(Pawn(Position.e2, Piece.WHITE), board.whitePieces[Position.e2])
        assertEquals(Pawn(Position.f2, Piece.WHITE), board.whitePieces[Position.f2])
        assertEquals(Pawn(Position.g2, Piece.WHITE), board.whitePieces[Position.g2])
        assertEquals(Pawn(Position.h2, Piece.WHITE), board.whitePieces[Position.h2])

        assertEquals(Rook(Position.a1, Piece.WHITE), board.whitePieces[Position.a1])
        assertEquals(Knight(Position.b1, Piece.WHITE), board.whitePieces[Position.b1])
        assertEquals(Bishop(Position.c1, Piece.WHITE), board.whitePieces[Position.c1])
        assertEquals(Queen(Position.d1, Piece.WHITE), board.whitePieces[Position.d1])
        assertEquals(King(Position.e1, Piece.WHITE), board.whitePieces[Position.e1])
        assertEquals(Bishop(Position.f1, Piece.WHITE), board.whitePieces[Position.f1])
        assertEquals(Knight(Position.g1, Piece.WHITE), board.whitePieces[Position.g1])
        assertEquals(Rook(Position.h1, Piece.WHITE), board.whitePieces[Position.h1])

        // black pawns
        assertEquals(Pawn(Position.a7, Piece.BLACK), board.blackPieces[Position.a7])
        assertEquals(Pawn(Position.b7, Piece.BLACK), board.blackPieces[Position.b7])
        assertEquals(Pawn(Position.c7, Piece.BLACK), board.blackPieces[Position.c7])
        assertEquals(Pawn(Position.d7, Piece.BLACK), board.blackPieces[Position.d7])
        assertEquals(Pawn(Position.e7, Piece.BLACK), board.blackPieces[Position.e7])
        assertEquals(Pawn(Position.f7, Piece.BLACK), board.blackPieces[Position.f7])
        assertEquals(Pawn(Position.g7, Piece.BLACK), board.blackPieces[Position.g7])
        assertEquals(Pawn(Position.h7, Piece.BLACK), board.blackPieces[Position.h7])

        assertEquals(Rook(Position.a8, Piece.BLACK), board.blackPieces[Position.a8])
        assertEquals(Knight(Position.b8, Piece.BLACK), board.blackPieces[Position.b8])
        assertEquals(Bishop(Position.c8, Piece.BLACK), board.blackPieces[Position.c8])
        assertEquals(Queen(Position.d8, Piece.BLACK), board.blackPieces[Position.d8])
        assertEquals(King(Position.e8, Piece.BLACK), board.blackPieces[Position.e8])
        assertEquals(Bishop(Position.f8, Piece.BLACK), board.blackPieces[Position.f8])
        assertEquals(Knight(Position.g8, Piece.BLACK), board.blackPieces[Position.g8])
        assertEquals(Rook(Position.h8, Piece.BLACK), board.blackPieces[Position.h8])
    }
}
