package de.senfdax.ufobat.shallowblue

import de.senfdax.ufobat.shallowblue.piece.King
import de.senfdax.ufobat.shallowblue.piece.Pawn
import de.senfdax.ufobat.shallowblue.piece.Queen
import de.senfdax.ufobat.shallowblue.piece.Rook
import de.senfdax.ufobat.shallowblue.Position.*
import de.senfdax.ufobat.shallowblue.Piece.Companion.BLACK
import de.senfdax.ufobat.shallowblue.Piece.Companion.WHITE
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

class BoardTest {

    @Test
    fun simpleMoveOnABoard() {
        val whitePieces = listOf(
            Pawn(Position.a2, Piece.WHITE)
        )
        val blackPieces = listOf(
            Pawn(Position.h7, Piece.BLACK)
        )
        val board = Board(whitePieces, blackPieces)
        val newBoard = board.applyMove(Move(Position.a2, Position.a4))
        assertTrue(newBoard is Board)
        if (newBoard is Board) {
            assertEquals(1, newBoard.whitePieces.count())
            assertEquals(1, newBoard.blackPieces.count())
            assertTrue(newBoard.whitePieces[Position.a4] is Pawn)
            assertTrue(newBoard.blackPieces[Position.h7] is Pawn)
            assertTrue(newBoard.whoIsNext == Piece.BLACK)
        }
    }

    @Test
    fun moveNotAccordingToRules() {
        val whitePieces = listOf(
            Pawn(Position.a2, Piece.WHITE)
        )
        val blackPieces = listOf(
            Pawn(Position.h7, Piece.BLACK)
        )
        val board = Board(whitePieces, blackPieces)
        val newBoard = board.applyMove(Move(Position.a2, Position.a6))
        assertTrue(newBoard is BoardMovementError)
    }

    @Test
    fun blackMakesASimpleCaptureOnABoard() {
        val whitePieces = listOf(
            Pawn(Position.a2, Piece.WHITE)
        )
        val blackPieces = listOf(
            Pawn(Position.b3, Piece.BLACK)
        )
        val board = Board(whitePieces, blackPieces, Piece.BLACK)
        val newBoard = board.applyMove(Move(Position.b3, Position.a2))
        assertTrue(newBoard is Board)
        if (newBoard is Board) {
            assertTrue(newBoard!!.blackPieces[Position.a2] is Pawn)
            assertEquals(0, newBoard!!.whitePieces.count())
            assertEquals(1, newBoard!!.blackPieces.count())
        }
    }

    @Test
    fun moveOfaNotExistingPieceOnABoard() {
        val board = Board(listOf(), listOf())
        val newBoard = board.applyMove(Move(Position.a2, Position.a4))
        assertTrue(newBoard is BoardMovementError)
    }

    @Test
    fun shortCastleMoveThatWorks() {
        val board = Board(
            listOf(King(Position.e1, Piece.WHITE), Rook(Position.a1, Piece.WHITE)),
            listOf()
        )
        val newBoard = board.applyMove(Move(Position.e1, Position.c1))
        assertTrue(newBoard is Board)
        if (newBoard is Board) {
            assertEquals(
                setOf(King(Position.c1, Piece.WHITE), Rook(Position.d1, Piece.WHITE)),
                newBoard!!.whitePieces.values.toSet()
            )
        }
    }

    @Test
    fun longCastleMoveThatWorks() {
        val board = Board(
            listOf(King(Position.e1, Piece.WHITE), Rook(Position.h1, Piece.WHITE)),
            listOf()
        )
        val newBoard = board.applyMove(Move(Position.e1, Position.g1))
        val expectation = setOf(Rook(Position.f1, Piece.WHITE), King(Position.g1,Piece.WHITE) )
        assertTrue(newBoard is Board)
        if (newBoard is Board) {
            assertEquals(expectation, newBoard!!.whitePieces.values.toSet())
        }
    }

    @Test
    fun longCastleMoveThatIsProhibited() {
        val castlingMove = Move(Position.e1, Position.g1)
        val board = Board(
            listOf(King(Position.e1, Piece.WHITE), Rook(Position.h1, Piece.WHITE)),
            listOf(),
            Piece.WHITE,
            Board.allAllowedCastlingMoves().filterNot { it == castlingMove }
        )
        val newBoard = board.applyMove(castlingMove)
        // val expectation = setOf(Rook(Position.f1, Piece.WHITE), King(Position.g1,Piece.WHITE) )
        // assertEquals(expectation, newBoard!!.whitePieces.values.toSet())
        assertTrue(newBoard is BoardMovementError)
    }

    @Test
    fun promotionMove() {
        val board = Board(listOf(), listOf(Pawn(Position.h2, Piece.BLACK)), Piece.BLACK)
        val newBoard = board.applyMove(Move(Position.h2, Position.h1, Promotion.Queen))
        assertTrue(newBoard is Board)
        if (newBoard is Board) {
            assertEquals(Queen(Position.h1, Piece.BLACK), newBoard!!.blackPieces[Position.h1])
        }
    }

    @Test
    fun promotionMoveWithTake() {
        val pawn = Pawn(g7, WHITE)
        val white = mapOf(pawn.position to pawn)
        val black = mapOf(h8 to Rook(h8, BLACK))

        val board = Board(white, black, WHITE)
        val newBoard = board.applyMove(Move(g7, h8, Promotion.Rook))
        assertTrue(newBoard is Board)
        if (newBoard is Board) {
            assertNotNull(newBoard)
            assertEquals(1, newBoard!!.whitePieces.values.count())
            assertEquals(Rook(h8, WHITE), newBoard!!.whitePieces[h8])
            assertEquals(0, newBoard!!.blackPieces.values.count())
        }
    }

    @Test
    fun enPassantMove() {
        val board = Board(
            listOf(Pawn(Position.b2, Piece.WHITE)),
            listOf(Pawn(Position.c4, Piece.BLACK)),
        )
        // white moves pawn 2 steps ahead
        val sut = board.applyMove(Move(Position.b2, Position.b4))
        assertNotNull(sut)
                assertTrue(sut is Board)
        if (sut is Board) {
            assertEquals(1, sut!!.blackPieces.count())
            assertEquals(1, sut!!.whitePieces.count())

            // black captures en passant
            val result = sut?.applyMove(Move(Position.c4, Position.b3))

            assertTrue(result is Board)
            if (result is Board) {
                assertNotNull(result)
                assertEquals(0, result!!.whitePieces.count())
                assertEquals(1, result!!.blackPieces.count())
                assertEquals(Pawn(Position.b3, Piece.BLACK), result!!.blackPieces[Position.b3])
            }
        }
    }

    @Test
    fun pinnedMovesAreDisallowed() {
        val sut = Board(
            listOf(King(Position.a3, Piece.WHITE), Pawn(Position.b3, Piece.WHITE)),
            listOf(Rook(Position.c3, Piece.BLACK)),
        )
        val move = Move(Position.b3, Position.b4)
        val invalidBoard = sut.applyMove(move)

        assertTrue(invalidBoard is BoardMovementError)
    }

    // TODO: tests for various castling not allowed szenarios
    // delayed for having the FEN parser available?
}
