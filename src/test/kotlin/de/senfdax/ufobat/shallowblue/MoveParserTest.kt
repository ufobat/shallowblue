package de.senfdax.ufobat.shallowblue

import de.senfdax.ufobat.shallowblue.Position.*
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class MoveParserTest {

    @Test
    fun parse_e2e4() {
        val result = MoveParser.parse("e2e4")
        assertEquals(Move(e2, e4), result)
        assertEquals("e2e4", result.toString())
    }

    @Test
    fun parse_e7e8q() {
        val result = MoveParser.parse("e7e8q")
        assertEquals(Move(e7, e8, Promotion.Queen), result)
        assertEquals("e7e8q", result.toString())
    }
}
