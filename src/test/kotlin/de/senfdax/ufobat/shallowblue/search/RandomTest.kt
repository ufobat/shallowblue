package de.senfdax.ufobat.shallowblue.search

import de.senfdax.ufobat.shallowblue.FENParser
import de.senfdax.ufobat.shallowblue.Piece
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test

class RandomTest {

    @Test
    fun makeRandomMove() {
        val board = FENParser.parse("""rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1""")
        val sut = Random()
        val (move, newBoard) = sut.getNextMove(board)
        assertNotNull(move)
        assertNotNull(newBoard)
        assertEquals(Piece.BLACK, newBoard.whoIsNext)
        println(move)

        val (secondMove, veryNewBoard) = sut.getNextMove(newBoard)
        assertEquals(Piece.WHITE, veryNewBoard.whoIsNext)
        println(secondMove)
    }
}
