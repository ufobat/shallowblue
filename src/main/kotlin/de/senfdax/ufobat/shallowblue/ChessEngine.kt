package de.senfdax.ufobat.shallowblue

class ChessEngine(
    private val searchAlgorithm: Search,
    private var currentBoard: Board,
    private val protocolResponder: ProtocolResponder,
    var beRandom: Boolean = false
    ) {



    fun respondTo(move: Move) {
        // apply move
        val opponentBoard = currentBoard.applyMove(move)
        if (opponentBoard is Board) {
            // think
            val (move, nextBoard) = searchAlgorithm.getNextMove(opponentBoard)
            // output
            protocolResponder.move(move)
            // dumpBoard(nextBoard)
            // update internal state
            currentBoard = nextBoard
        } else {
            protocolResponder.illegalMove(move)
        }
    }

//    private fun dumpBoard(board: Board) {
//        logger.trace("White: " + board.whitePieces.values.joinToString { it.toString() })
//        logger.trace("Black: " + board.blackPieces.values.joinToString { it.toString() })
//    }
}
