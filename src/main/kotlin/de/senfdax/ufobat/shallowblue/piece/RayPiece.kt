package de.senfdax.ufobat.shallowblue.piece

import de.senfdax.ufobat.shallowblue.Color
import de.senfdax.ufobat.shallowblue.Piece
import de.senfdax.ufobat.shallowblue.Position

abstract class RayPiece(position: Position, type: Color): Piece(position, type) {

    protected fun untilBlocked(
        it: Position?,
        block: MutableList<Boolean>,
        blockIdx: Int,
        me: Map<Position, Piece>,
        opponent: Map<Position, Piece>
    ) =
        it?.let {
            val r = if (block[blockIdx])
                null
            else if (me.containsKey(it)) {
                block[blockIdx] = true
                null
            } else if (opponent.containsKey(it)) {
                block[blockIdx] = true
                it
            } else
                it
            r
        }

}
