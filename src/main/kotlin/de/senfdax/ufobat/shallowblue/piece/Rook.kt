package de.senfdax.ufobat.shallowblue.piece

import de.senfdax.ufobat.shallowblue.Color
import de.senfdax.ufobat.shallowblue.Move
import de.senfdax.ufobat.shallowblue.Piece
import de.senfdax.ufobat.shallowblue.Position

class Rook(position: Position, type: Color) : RayPiece(position, type) {
    override fun pseudoLegalMoves(me: Map<Position, Piece>, opponent: Map<Position, Piece>): List<Move> {
        val block = mutableListOf( false, false, false, false )
        return (1..7)
            .flatMap { listOf(
                untilBlocked(Position.fromCoordinates(position.col, position.row + it), block, 0, me, opponent),
                untilBlocked(Position.fromCoordinates(position.col, position.row - it), block, 1, me, opponent),
                untilBlocked(Position.fromCoordinates(position.col + it, position.row), block, 2, me, opponent),
                untilBlocked(Position.fromCoordinates(position.col - it, position.row), block, 3, me, opponent)
            )
            }
            .filterNotNull()
            .map { Move(position, it) }
    }

    override fun moveTo(dest: Position): Piece = Rook(dest, type)
}
