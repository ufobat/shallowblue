package de.senfdax.ufobat.shallowblue.piece

import de.senfdax.ufobat.shallowblue.*

class Pawn(pos: Position, type: Color) : Piece(pos, type) {

    // what about black pawns?
    private fun movesToDestination(destination: Position): Set<Move> {
        val result = mutableSetOf<Move>()
        // pawns of the wrong color can not move to 0 or 7
        if (destination.row == 0 || destination.row == 7) {
            result.add(Move(position, destination, Promotion.Queen))
            result.add(Move(position, destination, Promotion.Rook))
            result.add(Move(position, destination, Promotion.Bishop))
            result.add(Move(position, destination, Promotion.Knight))
        } else {
            result.add(Move(position, destination))
        }
        return result
    }

    fun forward() : Position? {
        return if (isWhite()) this.position.north() else this.position.south()
    }

    fun backward() : Position? {
        return if (isWhite()) this.position.south() else this.position.north()
    }

    override fun pseudoLegalMoves(me: Map<Position, Piece>, opponent: Map<Position, Piece>): List<Move> {
        val result = mutableListOf<Move>()

        val oneStepAhead = forward()!!
        // step forward only if no-one is there
        if (!me.containsKey(oneStepAhead) && !opponent.containsKey(oneStepAhead)) {
            result.addAll(movesToDestination(oneStepAhead))
        }

        // regular attack: white left or black right
        oneStepAhead.west()?.also {
            // only if capture
            if (opponent.containsKey(it))
                result.addAll(movesToDestination(it))
        }

        // regular attack: white right or black left attack
        oneStepAhead.east()?.also {
            // only if capture
            if (opponent.containsKey(it))
                result.addAll(movesToDestination(it))
        }

        // potential en passant
        position.west()?.also {
            if (opponent.containsKey(it) && opponent[it] is Pawn) {
                result.add(Move(position, forward()!!.west()!!))
            }
        }
        position.east()?.also {
            if (opponent.containsKey(it) && opponent[it] is Pawn) {
                result.add(Move(position, forward()!!.east()!!))
            }
        }

        // only if one step ahead geht! dann
        if (!me.containsKey(oneStepAhead) && !opponent.containsKey(oneStepAhead)) {

            val twoSteps = if (isWhite() && position.row == 1) {
                oneStepAhead.north()!!
            } else if (isBlack() && position.row == 6) {
                oneStepAhead.south()!!
            } else null

            if (twoSteps != null && !me.containsKey(twoSteps) && !opponent.containsKey(twoSteps))
                result.add(Move(position, twoSteps))
        }

        return result
    }

    override fun moveTo(dest: Position) = Pawn(dest, type)
}

