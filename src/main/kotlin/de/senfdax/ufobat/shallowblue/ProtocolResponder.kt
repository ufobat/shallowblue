package de.senfdax.ufobat.shallowblue

import org.slf4j.Logger
import org.slf4j.LoggerFactory

class ProtocolResponder {
    private val logger: Logger = LoggerFactory.getLogger(this.javaClass)

    fun chess() {
        println("Chess")
        logger.trace("sending Chess... I am ready to serve")
    }

    fun illegalMove(move: Move) {
        println("Illegal move: $move")
        logger.warn("Ignoring an illegal move: $move")
    }

    fun error(msg: String, command: String) {
        println("Error ($msg): $command")
        logger.error("Error while handling $command, $msg")
    }

    fun move(move: Move) {
        println("move $move")
        logger.trace("moving my piece: $move")
    }

    fun feature() {
        // TODO setboard=1 && time=1
        println("feature ping=0 setboard=0 san=0 playother=0 time=0 sigint=0 sigterm=0 reuse=1 analyze=0 myname=shallowblue colors=0 ics=0 name=0 nps=0 debug=0 memory=0 smp=0 exclude=0 done=1")
        logger.trace("describing our features...")
    }
}
