package de.senfdax.ufobat.shallowblue

import de.senfdax.ufobat.shallowblue.search.Random
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import kotlin.system.exitProcess

class ProtocolHandler {
    private val logger: Logger = LoggerFactory.getLogger(this.javaClass)
    private val protocolResponder = ProtocolResponder()
    private var engine = newEngine()

    fun process() {
        protocolResponder.chess()
        // Todo:
        /*
         xboard
         protover2
         force
         new
         random
         level 40 5 0
         post
         hard
         new
         */
        for (line in generateSequence(::readLine)) {
            logger.trace("processing $line")
            when {
                line.matches(MoveParser.moveRegex) -> handleReceivedMove(line)
                else -> handleReceivedCommand(line)
            }
        }
    }

    private fun handleReceivedCommand(command: String) {
        when (command) {
            "xboard" -> {}
            "new" -> { engine = newEngine() }
            "random" -> engine.beRandom = true
            "protover 2" -> protocolResponder.feature()
            "quit" -> exitProcess(0)
            else -> logger.warn("command $command not yet implemented!")
        }
    }

    private fun newEngine(): ChessEngine {
        return ChessEngine(
            Random(),
            // NegaMax(MaterialOnly()),
            FENParser.parse("""rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1"""),
            protocolResponder
        )
    }

    private fun handleReceivedMove(command: String) {
        val currentMove = MoveParser.parse(command)
        if (currentMove != null)
            engine.respondTo(currentMove)
        else
            protocolResponder.error("unknown command", command)
    }
}
