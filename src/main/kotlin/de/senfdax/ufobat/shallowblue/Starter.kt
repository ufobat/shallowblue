package de.senfdax.ufobat.shallowblue

class Starter {
    companion object {
        @JvmStatic
        fun main(argv: Array<String>) {
            val protocolHandler = ProtocolHandler()
            protocolHandler.process()
        }
    }
}
