package de.senfdax.ufobat.shallowblue

enum class Promotion(val shortName: String?) {
    NONE(""), Queen("q"), Rook("r"), Bishop("b"), Knight("n");

    companion object {
        fun byShortName(s: String?): Promotion? = values().firstOrNull { it.shortName == s }
    }
}
