package de.senfdax.ufobat.shallowblue


inline fun <reified T : Enum<T>, V> ((T) -> V).find(value: V): T? {
    return enumValues<T>().firstOrNull { this(it) == value }
}

object MoveParser {

    val moveRegex = """([a-h][1-8])([a-h][1-8])([qrkb])?""".toRegex()
    fun parse(move: String) : Move? {
        val match = moveRegex.matchEntire(move)
        if(match != null) {
            val src = match.groupValues[1]
            val dst = match.groupValues[2]
            val pro = match.groupValues[3]

            try {
                val srcPos = Position.valueOf(src)
                val dstPos = Position.valueOf(dst)
                val proPro = Promotion.byShortName(pro) ?: Promotion.NONE
                return Move(srcPos, dstPos, proPro)
            } catch (e: Exception) {
                return null
            }
        }
        return null

    }
}
