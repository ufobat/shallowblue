package de.senfdax.ufobat.shallowblue.search

import de.senfdax.ufobat.shallowblue.Board
import de.senfdax.ufobat.shallowblue.Move
import de.senfdax.ufobat.shallowblue.Search

class Random: Search {

    override fun getNextMove(board: Board): Pair<Move,Board> {
        val allowedMoves : Map<Move, Board> = board
            .myPseudoLegalMoves()
            .associateWith { board.applyMove(it) }
            .filterValues { it != null } as Map<Move, Board>

        return allowedMoves.toList().random()
    }
}
