package de.senfdax.ufobat.shallowblue

interface Search {
    fun getNextMove(board: Board): Pair<Move,Board>
}
