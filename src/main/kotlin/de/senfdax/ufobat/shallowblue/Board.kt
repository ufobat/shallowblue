package de.senfdax.ufobat.shallowblue

import de.senfdax.ufobat.shallowblue.piece.*

sealed class BoardMovementResult
data class BoardMovementError(val message: String) : BoardMovementResult()

class Board(
    val whitePieces: Map<Position, Piece>,
    val blackPieces: Map<Position, Piece>,
    val whoIsNext: Color = Piece.WHITE,
    private val allowedCastlingMoves: List<Move> = allAllowedCastlingMoves(),
    private val allowedEnPassantAttack: Position? = null
    ): BoardMovementResult() {

    companion object {
        val WhiteQueenCastlingMove = Move(Position.e1, Position.c1)
        val WhiteKingCastlingMove = Move(Position.e1, Position.g1)
        val BlackQueenCastlingMove = Move(Position.e8, Position.c1)
        val BlackKingCastlingMove = Move(Position.e8, Position.g8)
        fun allAllowedCastlingMoves() = listOf(WhiteKingCastlingMove, WhiteQueenCastlingMove, BlackKingCastlingMove, BlackQueenCastlingMove)
    }

    constructor(
        whitePieces: List<Piece>,
        blackPieces: List<Piece>,
        whoIsNext: Color = Piece.WHITE,
        allowedCastlingMoves: List<Move> = allAllowedCastlingMoves(),
        allowedEnPassantAttack: Position? = null
    ) : this(
        whitePieces.associateBy { it.position },
        blackPieces.associateBy { it.position },
        whoIsNext,
        allowedCastlingMoves,
        allowedEnPassantAttack
    )

    private fun isCastlingMoveAllowed(move: Move) = allowedCastlingMoves.contains(move)

    fun applyMove(move: Move) : BoardMovementResult {
        // Step 1 - Who's turn?
        val (me, opponent) = if(whoIsNext == Piece.WHITE)
            whitePieces to blackPieces
        else
            blackPieces to whitePieces

        // Step 2 - find out moving piece?
        val piece = me[move.from] ?: return BoardMovementError("move $move, there is not even a piece")
        // Step 3 - is a pseudoLegalMove
        if (piece.pseudoLegalMoves(me, opponent).contains(move)) {

            // Step 4 - Detect Type of Move and execute it
            return makeMove(piece, move, me, opponent)
        }
        return BoardMovementError("move $move is not allowed - not a pseudo legal move")
    }

    private fun nextBoard(
        newMe: Map<Position, Piece>,
        newOpponent: Map<Position, Piece>,
        newAllowedCastlingMoves: List<Move>,
        newAllowedEnPassantAttack: Position?,
    ) = if (whoIsNext == Piece.WHITE)
        Board(newMe, newOpponent, !whoIsNext, newAllowedCastlingMoves, newAllowedEnPassantAttack)
    else
        Board(newOpponent, newMe, !whoIsNext, newAllowedCastlingMoves, newAllowedEnPassantAttack)

    private fun makeMove(piece: Piece, move: Move, me: Map<Position, Piece>, opponent: Map<Position, Piece>) : BoardMovementResult {
        val opponentPiece: Piece? = opponent[move.dest]
        var newMe: Map<Position, Piece>?
        var newOpponent: Map<Position, Piece>?
        var newAllowedEnPassantAttack: Position? = null

        // Detect Move Types
        if (move.isCastling() && piece is King) {
            // 1) Castling
             if (!isCastlingMoveAllowed(move))
                 return BoardMovementError("move $move not allowed, castling not allowed now")
            // lets go! Move King && Move Rook
            val (rookStartPos, rookDestPos) = getCastlingRookMove(move)
            val rook = me[rookStartPos] as Rook
            newMe = me.filterNot { it.key == move.from || it.key == rookStartPos } +
                    setOf(rook.moveTo(rookDestPos), piece.moveTo(move.dest)).associateBy { it.position }
            newOpponent = opponent
        }
        else if(piece is Pawn && move.dest == allowedEnPassantAttack) {
            // 2) En passant
            val opponentPawnPosition = piece.moveTo(move.dest).backward()
            newOpponent = opponent.filterNot { it.key == opponentPawnPosition }
            newMe = me.filterNot { it.key == move.from } + listOf(piece.moveTo(move.dest)).associateBy { it.position }
        }
        else if (move.promotionTo != Promotion.NONE && opponentPiece == null) {
            // 3) Promotion without capture
            newMe = me.filterNot { it.key == move.from } +
                    mapOf(move.dest to promotedPiece(move.promotionTo, move.dest, piece.type))
            newOpponent = opponent
        }
        else if (move.promotionTo != Promotion.NONE && opponentPiece != null) {
            // 3b) Promotion with capture
            newMe = me.filterNot { it.key == move.from } +
                    mapOf(move.dest to promotedPiece(move.promotionTo, move.dest, piece.type))
            newOpponent = opponent.filterNot { it.key == move.dest }
        }
        else if ( opponentPiece != null ) {
            // 4) Normal Capture
            newOpponent = opponent.filterNot { it.key == move.dest }
            newMe = me.filterNot { it.key == move.from } + listOf(piece.moveTo(move.dest)).associateBy { it.position }
        }
        else {
            // 5) Normal Move
            newMe = me.filterNot { it.key == move.from } + listOf(piece.moveTo(move.dest)).associateBy { it.position }
            newOpponent = opponent
            if (piece is Pawn && move.manhattanDistance() == 2) {
                // piece is before the move
                newAllowedEnPassantAttack = piece.forward()
            }
        }

        // 6) After Moves: Was Figure Pinned?!
        // firstOrNull is only for Tests...
        newMe.values.firstOrNull { it is King }?.let { myKing ->
            newOpponent.values.forEach { opponentPiece ->
                opponentPiece.pseudoLegalMoves(newOpponent, newMe).forEach {
                    if (it.dest == myKing.position) {
                        // king is killable -> illegal move
                        return BoardMovementError("move $move would kill your king")
                    }
                }
            }
        }

        // calculate allowed Castling Moves
        val newAllowedCastlingMoves = when (move.from) {
            Position.e1 -> allowedCastlingMoves.filterNot { it == WhiteKingCastlingMove || it == WhiteQueenCastlingMove }
            Position.e8 -> allowedCastlingMoves.filterNot { it == BlackKingCastlingMove || it == BlackQueenCastlingMove }
            Position.a1 -> allowedCastlingMoves.filterNot { it == WhiteQueenCastlingMove }
            Position.h1 -> allowedCastlingMoves.filterNot { it == WhiteKingCastlingMove }
            Position.a8 -> allowedCastlingMoves.filterNot { it == BlackQueenCastlingMove }
            Position.h8 -> allowedCastlingMoves.filterNot { it == BlackKingCastlingMove }
            else        -> allowedCastlingMoves
        }

        return nextBoard(newMe, newOpponent, newAllowedCastlingMoves, newAllowedEnPassantAttack)
    }

    private fun promotedPiece(promotionTo: Promotion, dest: Position, color: Color) = when(promotionTo) {
        Promotion.Knight -> Knight(dest, color)
        Promotion.Bishop -> Bishop(dest, color)
        Promotion.Queen -> Queen(dest, color)
        Promotion.Rook -> Rook(dest, color)
        else -> error("thats not even possible")
    }

    private fun getCastlingRookMove(kingMove: Move) = when (kingMove.dest) {
        Position.c1 -> Position.a1 to Position.d1
        Position.g1 -> Position.h1 to Position.f1
        Position.c8 -> Position.a8 to Position.d8
        Position.g8 -> Position.h8 to Position.f8
        else -> throw  IllegalArgumentException("$kingMove is no CastlingMove")
    }

    fun myPseudoLegalMoves() : List<Move> {
        val (me, opponent) = if(whoIsNext == Piece.WHITE)
            whitePieces to blackPieces
        else
            blackPieces to whitePieces

        return me.flatMap { (_, piece) ->
            piece.pseudoLegalMoves(me, opponent)
        }
    }
}
