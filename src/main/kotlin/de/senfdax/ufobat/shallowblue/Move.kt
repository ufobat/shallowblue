package de.senfdax.ufobat.shallowblue

import kotlin.math.absoluteValue

data class Move(
    val from: Position,
    val dest: Position,
    val promotionTo: Promotion = Promotion.NONE
) {

    fun isCastling(): Boolean {
        if (from == Position.e1 && (dest == Position.c1 || dest == Position.g1))
            return true
        if (from == Position.e8 && (dest == Position.c8 || dest == Position.g8))
            return true
        return false
    }

    fun manhattanDistance(): Int {
        return (from.row - dest.row).absoluteValue + (from.col - dest.col).absoluteValue
    }

    override fun toString() = from.name + dest.name + promotionTo.shortName
}
