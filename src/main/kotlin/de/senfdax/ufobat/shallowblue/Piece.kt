package de.senfdax.ufobat.shallowblue

typealias Color = Boolean
abstract class Piece(val position: Position, val type: Color) {

    companion object {
        const val BLACK: Color = true
        const val WHITE: Color = false
    }
    abstract fun pseudoLegalMoves(
        me: Map<Position, Piece> = mapOf(),
        opponent: Map<Position, Piece> = mapOf()
    ): List<Move>
    abstract fun moveTo(dest: Position): Piece

    fun isBlack() : Boolean { return type == BLACK }
    fun isWhite() : Boolean { return type == WHITE }
    private fun getColor(): String = if (isWhite()) "white" else "black"

    override fun equals(other: Any?): Boolean {
        if (other != null && other is Piece) {
            return this.position == other.position && this.type == other.type
        }
        return false
    }

    override fun hashCode() = type.hashCode() + position.hashCode()
    override fun toString() = this.javaClass.simpleName + "{${getColor()}, $position}"
}
