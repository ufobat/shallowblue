package de.senfdax.ufobat.shallowblue

import de.senfdax.ufobat.shallowblue.piece.*

object FENParser {
    val numbers = (1..8).map { it.toString() }
    val figureSymbol = Regex("""[pnbrqk]""", RegexOption.IGNORE_CASE)

    // split: lists usually destructure only 5 elements...
    private operator fun <E> List<E>.component6() = this[5]

    fun parse(fen: String) : Board {
        // starts at a8 -> h8, a7 -> h7, ... , a1 -> a8
        var row = 7
        var col = 0
        val blackPieces = mutableListOf<Piece>()
        val whitePieces = mutableListOf<Piece>()
        val (positions, whoIsNext, castlings, enPassant, halfMove, fullMove) = fen.split(' ')

        positions.toCharArray().map { it.toString() }.forEach {
            when {
                it in numbers -> {
                    // move current position
                    col += it.toInt()
                    if (col > 8) col = 1
                }
                it == "/"                -> {
                    // next row
                    row--
                    col = 0
                }
                figureSymbol.matches(it) -> {
                    val position = Position.fromCoordinates(col, row)!!
                    col++
                    val color = if(it.lowercase() == it) Piece.BLACK else Piece.WHITE
                    val piece = when (it.lowercase()) {
                        "p" -> Pawn(position, color)
                        "n" -> Knight(position, color)
                        "b" -> Bishop(position, color)
                        "r" -> Rook(position, color)
                        "q" -> Queen(position, color)
                        "k" -> King(position, color)
                        else -> null
                    }
                    if (piece != null && color == Piece.WHITE)
                        whitePieces.add(piece)
                    else if (piece != null && color == Piece.BLACK)
                        blackPieces.add(piece)
                }
                else -> {
                    throw IllegalArgumentException("can not parse $fen at sign $it")
                }
            }
        }

        val nextColor = if (whoIsNext == "w") Piece.WHITE else Piece.BLACK
        val allowedCastlingMoves: List<Move> = castlings.toCharArray().map { when(it) {
            'k' -> Board.WhiteKingCastlingMove
            'q' -> Board.WhiteQueenCastlingMove
            'K' -> Board.BlackKingCastlingMove
            'Q' -> Board.BlackQueenCastlingMove
            else -> null
        } }.filterNotNull()

        val allowedEnPassantAttack = if (enPassant != "-") Position.valueOf(enPassant) else null

        // TODO halfMove: https://en.wikipedia.org/wiki/Fifty-move_rule
        // ignore fullMoves for now

        return Board(whitePieces, blackPieces, nextColor, allowedCastlingMoves, allowedEnPassantAttack)
    }
}
